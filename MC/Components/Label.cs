﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC {
    class Label : Component {
        private readonly string text;

        public Label((int, int) position, (int, int) size, Renderer renderer, Colors colors,
            string text) : base(position, size, renderer, colors) => this.text = text;

        public override void Draw() {
            WritableData wData = new WritableData {
                textColor = colors.Text,
                backgroundColor = colors.Background,
                data = text,
                position = position
            };

            renderer.Write(wData);
        }
    }
}
