﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC {
    abstract class Window : Component {
        protected List<Row> rows;

        public Window((int, int) position, (int, int)size, Renderer renderer, Colors colors) :
            base(position, size, renderer, colors) {
        }

        public override void Initialize() => rows = CreateRows();

        protected abstract List<Row> CreateRows();

        protected abstract void SelectNextEntry();
        protected abstract void SelectPreviousEntry();
        public abstract void ActivateFirstRow();
    }
}
