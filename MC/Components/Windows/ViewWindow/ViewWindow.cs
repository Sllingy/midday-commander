﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MC {
    class ViewWindow : Window {
        public DirectoryInfo selectedDirectory;
        private readonly Application app;

        public ViewWindow((int, int) position, (int, int) size, Renderer renderer, Colors colors, DirectoryInfo directory, Application app) :
            base(position, size, renderer, colors) {
            selectedDirectory = directory;
            this.app = app;
            Initialize();
        }

        protected override List<Row> CreateRows() {
            var rows = new List<Row>();

            foreach (DirectoryInfo directoryInfo in selectedDirectory.EnumerateDirectories()) {
                if (rows.Count < size.Y - 3)
                    rows.Add(new DirectoryRow(directoryInfo, size.X - 2));
            }

            foreach (FileInfo fileInfo in selectedDirectory.EnumerateFiles()) {
                if (rows.Count < size.Y - 3)
                    rows.Add(new FileRow(fileInfo, size.X - 2));
            }

            while (rows.Count < size.Y - 3) {
                rows.Add(new EmptyDataRow(size.X - 2));
            }

            return rows;
        }

        public override void ActivateFirstRow() {
            List<Row> nonEmptyRows = new List<Row>();
            foreach (Row row in rows) {
                if (!(row is EmptyDataRow))
                    nonEmptyRows.Add(row);
            }

            if (nonEmptyRows.Count != 0) {
                rows.Find(X => !(X is EmptyDataRow)).Activate();
            }
        }

        private void DrawRows() {
            WritableData wData = new WritableData {
                textColor = colors.Text,
                backgroundColor = colors.Background
            };

            string s = "Name";
            wData.data = s.PadLeft((size.X - s.Length) / 4 + 3).PadRight(size.X / 2);
            wData.position = (position.X + 1, position.Y + 1);
            renderer.Write(wData);

            wData.data = " Size   ";
            wData.position = (position.X + size.X / 2 + 1, position.Y + 1);
            renderer.Write(wData);

            wData.data = "Last Access".PadRight(size.X / 2 - 2 - 8);
            wData.position = (position.X + size.X / 2 + 9, position.Y + 1);
            renderer.Write(wData);

            for (int i = 0; i < rows.Count; i++) {
                wData.position = (position.X + 1, position.Y + 2 + i);

                wData.backgroundColor = rows[i].isActive ? isActive ? colors.Active : colors.Inactive : colors.Background;

                wData.data = rows[i].View();

                renderer.Write(wData);
            }
        }

        protected override void SelectNextEntry() {
            List<Row> nonEmptyRows = new List<Row>();
            foreach (Row row in rows) {
                if (!(row is EmptyDataRow))
                    nonEmptyRows.Add(row);
            }

            int index = nonEmptyRows.IndexOf(rows.Find(x => x.isActive));

            if (nonEmptyRows.Count != 0) {
                if (index != nonEmptyRows.Count - 1) {
                    nonEmptyRows[index].DeActivate();
                    nonEmptyRows[index + 1].Activate();
                } else {
                    nonEmptyRows[index].DeActivate();
                    nonEmptyRows.FirstOrDefault().Activate();
                }
            }
        }

        protected override void SelectPreviousEntry() {
            List<Row> nonEmptyRows = new List<Row>();
            foreach (Row row in rows) {
                if (!(row is EmptyDataRow))
                    nonEmptyRows.Add(row);
            }

            int index = nonEmptyRows.IndexOf(rows.Find(x => x.isActive));

            if (nonEmptyRows.Count != 0) {
                if (index == 0) {
                    nonEmptyRows[index].DeActivate();
                    nonEmptyRows[nonEmptyRows.Count - 1].Activate();
                } else {
                    nonEmptyRows[index].DeActivate();
                    nonEmptyRows[index - 1].Activate();
                }
            }
        }

        private void SelectSubDirectory() {
            //first enumerate directories, then enumerate files!
            List<DataRow> nonEmptyRows = new List<DataRow>();
            foreach (DataRow row in rows) {
                if (!(row is EmptyDataRow))
                    nonEmptyRows.Add(row);
            }

            if (nonEmptyRows.Count != 0) {
                if (rows[rows.IndexOf(rows.Find(x => x.isActive))] is DirectoryRow directoryRow) {
                    try {
                        if (directoryRow.fileInfo is DirectoryInfo directoryInfo)
                            directoryInfo.EnumerateFileSystemInfos();
                        selectedDirectory = (DirectoryInfo)directoryRow.fileInfo;
                        rows = CreateRows();
                        ActivateFirstRow();
                    } catch (Exception) {
                    }
                }
            }
        }

        private void EditFile() {
            List<DataRow> nonEmptyRows = new List<DataRow>();
            foreach (DataRow row in rows) {
                if (!(row is EmptyDataRow))
                    nonEmptyRows.Add(row);
            }

            if (nonEmptyRows.Count != 0) {
                if (rows[rows.IndexOf(rows.Find(x => x.isActive))] is FileRow fileRow) {
                    app.CreateFileEditorWindow((FileInfo)fileRow.fileInfo);
                }
            }
        }

        public void RecreateRows() {
            rows = CreateRows();
            ActivateFirstRow();
        }

        private void SelectParentDirectory() {
            if (selectedDirectory.Parent != null) {
                selectedDirectory = selectedDirectory.Parent;
                RecreateRows();
            }
        }

        private void CreateDirectory(int name) {
            if (Directory.Exists(selectedDirectory.FullName + @"\test" + name)) {
                CreateDirectory(++name);
            } else {
                Directory.CreateDirectory(selectedDirectory.FullName + @"\test" + name);
                rows = CreateRows();
                ActivateFirstRow();
            }
        }

        private void CreateFile(int name) {
            if (File.Exists(selectedDirectory.FullName + @"\test" + name + ".txt")) {
                CreateFile(++name);
            } else {
                File.Create(selectedDirectory.FullName + @"\test" + name + ".txt");
                rows = CreateRows();
                ActivateFirstRow();
            }
        }

        public void CreateFile(string name) {
            File.Create(selectedDirectory.FullName + @"\" + name);
            app.RefreshWindows();
        }

        public void CreateDirectory(string name) {
            Directory.CreateDirectory(selectedDirectory.FullName + @"\" + name);
            app.RefreshWindows();
        }

        public void DeleteFileSystemInfo() {
            List<DataRow> nonEmptyRows = new List<DataRow>();
            foreach (DataRow row in rows) {
                if (!(row is EmptyDataRow))
                    nonEmptyRows.Add(row);
            }

            int index = nonEmptyRows.IndexOf(nonEmptyRows.Find(x => x.isActive));

            try {
                if (nonEmptyRows.Count != 0) {
                    if (nonEmptyRows[index].fileInfo is FileInfo) {
                        File.Delete(selectedDirectory.FullName + @"\" + nonEmptyRows[index].name);
                        app.RefreshWindows();
                    }
                    if (nonEmptyRows[index].fileInfo is DirectoryInfo) {
                        Directory.Delete(selectedDirectory.FullName + @"\" + nonEmptyRows[index].name, true);
                        app.RefreshWindows();
                    }

                    rows = CreateRows();
                    ActivateFirstRow();
                }
            } catch (Exception) {
            }
        }

        public FileSystemInfo GetFileSystemInfo() {
            List<DirectoryInfo> directories = selectedDirectory.EnumerateDirectories().ToList();
            int selectedIndex = rows.IndexOf(rows.Find(x => x.isActive));

            if (selectedIndex > directories.Count - 1) {
                List<FileInfo> files = selectedDirectory.EnumerateFiles().ToList();
                int fileIndex = selectedIndex - (directories.Count - 1);
                FileInfo file = files[fileIndex - 1];
                return file;
            } else {
                DirectoryInfo directory = directories[rows.IndexOf(rows.Find(x => x.isActive))];
                return directory;
            }
        }

        public override void Draw() {
            DrawRows();

            if (isActive) {
                renderer.DrawComponentBorders(position, size, colors.Active);

            } else {
                renderer.DrawComponentBorders(position, size, colors.Inactive);

            }
        }

        public override void HandleInput(ConsoleKeyInfo keyInfo) {
            if (isActive) {
                switch (keyInfo.Key) {
                    case ConsoleKey.DownArrow:
                        SelectNextEntry();
                        break;
                    case ConsoleKey.UpArrow:
                        SelectPreviousEntry();
                        break;
                    case ConsoleKey.Enter:
                        SelectSubDirectory();
                        break;
                    case ConsoleKey.Backspace:
                        SelectParentDirectory();
                        break;
                    case ConsoleKey.F1:
                        app.CreateFileCreationDialogWindow(true);
                        break;
                    case ConsoleKey.F2:
                        app.CreateFileCreationDialogWindow(false);
                        break;
                    case ConsoleKey.F4:
                        DeleteFileSystemInfo();
                        break;
                    case ConsoleKey.F6:
                        EditFile();
                        break;
                }
            }
        }
    }
}
