﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace MC {
    class FileEditor : Component {
        private readonly FileInfo fileInfo;
        private BufferChar[,] buffer;
        private (int X, int Y) activeIndex = (0, 0);
        private readonly Application app;

        public FileEditor((int, int) position, (int, int) size, Renderer renderer, Colors colors, FileInfo fileInfo, Application app) :
            base(position, size, renderer, colors) {
            this.fileInfo = fileInfo;
            this.app = app;
            LoadFile();
            app.CreateLabels(new List<string> { "Arrows - Move Cursor", "F9 - Exit Without Saving", "Escape - Save to File and Exit" });
        }

        private bool CheckAvailability() {
            try {
                string data = File.ReadAllText(fileInfo.FullName);
                return true;
            } catch (Exception) {
                return false;
            }
        }

        public void SaveToFile() {
            string data = "";
            for (int i = 0; i < buffer.GetLength(1); i++) {
                for (int n = 0; n < buffer.GetLength(0); n++) {
                    data += buffer[n, i].Data;
                }
            }

            File.WriteAllText(fileInfo.FullName, data);
        }

        public override void Close() {
            app.CreateLabels(new List<string> { "F1 - Create File", "F2 - Create Directory", "F3 - Copy File/Directory",
                "F4 - Delete File/Directory", "F5 - Change Drive", "F6 - Edit File",
                "F10 - Exit", "F12 - TOTRIS"});
        }

        public char GetWriteableCharacter(char input) {
            try {
                return Regex.Replace(input.ToString(), @"[^\w\.@-]", "", RegexOptions.None, TimeSpan.FromSeconds(1.5))[0];
            } catch (RegexMatchTimeoutException) {
                return Char.MaxValue;
            }
        }

        public void LoadFile() {
            string data = "";
            if (CheckAvailability()) {
                data = File.ReadAllText(fileInfo.FullName);
            }

            buffer = new BufferChar[size.X - 2, size.Y - 2];

            int dataIndex = 0;
            int dataSize = data.Length;

            for (int i = 0; i < buffer.GetLength(1); i++) {
                for (int n = 0; n < buffer.GetLength(0); n++) {
                    buffer[n, i].BackgroundColor = colors.Background;
                    buffer[n, i].TextColor = colors.Text;
                    if (dataIndex < dataSize) {
                        buffer[n, i].Data = data[dataIndex];
                        dataIndex++;
                    }
                }
            }
        }

        private void WriteCharacter(char input) {
            char c = GetWriteableCharacter(input);
            if (c != Char.MaxValue) {
                buffer[activeIndex.X, activeIndex.Y].Data = c;
                buffer[activeIndex.X, activeIndex.Y].BackgroundColor = colors.Background;

                Move(Direction.RIGHT);
            }
        }

        private void RemoveCharacter(Direction direction) {
            buffer[activeIndex.X, activeIndex.Y].Data = ' ';
            buffer[activeIndex.X, activeIndex.Y].BackgroundColor = colors.Background;

            Move(direction);
        }

        private void Move(Direction direction) {
            switch (direction) {
                case Direction.UP:
                    if (activeIndex.Y != 0) {
                        activeIndex.Y--;
                    } else {        
                        activeIndex.Y = buffer.GetLength(1) - 1;
                    }               
                    break;          
                case Direction.DOWN:
                    if (activeIndex.Y != buffer.GetLength(1) - 1) {
                        activeIndex.Y++;
                    } else {        
                        activeIndex.Y = 0;
                    }
                    break;
                case Direction.LEFT:
                    if (activeIndex.X != 0) {
                        activeIndex.X--;
                    } else {
                        activeIndex.X = buffer.GetLength(0) - 1;
                    }
                    break;
                case Direction.RIGHT:
                    if (activeIndex.X != buffer.GetLength(0) - 1) {
                        activeIndex.X++;
                    } else {
                        activeIndex.X = 0;
                    }
                    break;
            }
        }

        public override void Draw() {
            renderer.DrawComponentBorders(position, size, colors.Active);
            BufferChar bufferChar;

            for (int i = 0; i < buffer.GetLength(0); i++) {
                for (int n = 0; n < buffer.GetLength(1); n++) {
                    bufferChar = buffer[i, n];
                    if (activeIndex.X == i && activeIndex.Y == n)
                        bufferChar.BackgroundColor = colors.Active;
                    renderer.WriteCharacter(bufferChar, position.X + i + 1, position.Y + n + 1);
                }
            }
        }

        public override void HandleInput(ConsoleKeyInfo keyInfo) {
            if (isActive) {
                switch (keyInfo.Key) {
                    case ConsoleKey.DownArrow:
                        Move(Direction.DOWN);
                        break;
                    case ConsoleKey.UpArrow:
                        Move(Direction.UP);
                        break;
                    case ConsoleKey.LeftArrow:
                        Move(Direction.LEFT);
                        break;
                    case ConsoleKey.RightArrow:
                        Move(Direction.RIGHT);
                        break;
                    case ConsoleKey.Backspace:
                        RemoveCharacter(Direction.LEFT);
                        break;
                    case ConsoleKey.Spacebar:
                        RemoveCharacter(Direction.RIGHT);
                        break;
                    default:
                        WriteCharacter(keyInfo.KeyChar);
                        break;
                }
            }
            base.HandleInput(keyInfo);
        }
    }
}
