﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC {
    abstract class DialogWindow : Window {
        public DialogWindow((int, int) position, (int, int) size, Renderer renderer, Colors colors) :
            base(position, size, renderer, colors) {
        }

        protected override List<Row> CreateRows() {
            var rows = new List<Row>();

            while (rows.Count < size.Y - 2) {
                rows.Add(new EmptyTextRow(size.X - 2));
            }

            return rows;
        }

        public override void ActivateFirstRow() {
            List<Row> nonEmptyRows = new List<Row>();
            foreach (Row row in rows) {
                if (!(row is EmptyTextRow))
                    nonEmptyRows.Add(row);
            }

            if (nonEmptyRows.Count != 0) {
                rows.Find(X => !(X is EmptyTextRow)).Activate();
            }
        }

        protected override void SelectNextEntry() {
            List<Row> nonEmptyRows = new List<Row>();
            foreach (Row row in rows) {
                if (!(row is EmptyTextRow))
                    nonEmptyRows.Add(row);
            }

            int index = nonEmptyRows.IndexOf(rows.Find(x => x.isActive));

            if (nonEmptyRows.Count != 0) {
                if (index != nonEmptyRows.Count - 1) {
                    nonEmptyRows[index].DeActivate();
                    nonEmptyRows[index + 1].Activate();
                } else {
                    nonEmptyRows[index].DeActivate();
                    nonEmptyRows.FirstOrDefault().Activate();
                }
            }
        }

        protected override void SelectPreviousEntry() {
            List<Row> nonEmptyRows = new List<Row>();
            foreach (Row row in rows) {
                if (!(row is EmptyTextRow))
                    nonEmptyRows.Add(row);
            }

            int index = nonEmptyRows.IndexOf(rows.Find(x => x.isActive));

            if (nonEmptyRows.Count != 0) {
                if (index == 0) {
                    nonEmptyRows[index].DeActivate();
                    nonEmptyRows[nonEmptyRows.Count - 1].Activate();
                } else {
                    nonEmptyRows[index].DeActivate();
                    nonEmptyRows[index - 1].Activate();
                }
            }
        }

        public override void Draw() {
            renderer.DrawComponentBorders(position, size, colors.Active);

            WritableData wData = new WritableData {
                backgroundColor = colors.Background,
                textColor = colors.Text
            };


            for (int i = 0; i < rows.Count; i++) {
                wData.position = (position.X + 1, position.Y + 1 + i);

                wData.backgroundColor = rows[i].isActive ? colors.Active : colors.Background;

                wData.data = rows[i].View();
                renderer.Write(wData);
            }
        }

        public override void HandleInput(ConsoleKeyInfo keyInfo) {
            if (isActive) {
                switch (keyInfo.Key) {
                    case ConsoleKey.DownArrow:
                        SelectNextEntry();
                        break;
                    case ConsoleKey.UpArrow:
                        SelectPreviousEntry();
                        break;
                    case ConsoleKey.Enter:
                        Action();
                        break;
                }
            }
        }

        protected virtual void Action() {

        }
    }
}
