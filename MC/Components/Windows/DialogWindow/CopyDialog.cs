﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MC {
    class CopyDialog : DialogWindow {
        private readonly FileSystemInfo selectedFileSystemInfo;
        private readonly List<DirectoryInfo> directories;
        private readonly Application app;

        public CopyDialog((int, int) position, (int, int) size, Renderer renderer, Colors colors,
            FileSystemInfo selectedFileSystemInfo, List<DirectoryInfo> directories, Application app) : base(
            position, size, renderer, colors) {
            this.selectedFileSystemInfo = selectedFileSystemInfo;
            this.directories = directories;
            this.app = app;
            Initialize();
        }

        protected override List<Row> CreateRows() {
            var rows = new List<Row>();

            foreach (var directory in directories) {
                rows.Add(new DirectoryRow(directory, size.X - 2));
            }

            while (rows.Count < size.Y - 3) {
                rows.Add(new EmptyDataRow(size.X - 2));
            }

            return rows;
        }

        protected override void Action() {
            if (selectedFileSystemInfo is DirectoryInfo directoryInfo) {
                string s = Path.Combine(directories[rows.IndexOf(rows.Find(x => x.isActive))].FullName, selectedFileSystemInfo.Name);
                Directory.CreateDirectory(s);
                DirectoryInfo directory = new DirectoryInfo(s);
                try {
                    CopyAll(directoryInfo, directory);
                } catch (Exception) {
                }
            }

            if (selectedFileSystemInfo is FileInfo FileInfo) {
                Copy(FileInfo, directories[rows.IndexOf(rows.Find(x => x.isActive))]);
            }

            app.RefreshWindows();
        }

        private void CopyAll(DirectoryInfo source, DirectoryInfo target) {
            foreach (FileInfo sfi in source.GetFiles()) {
                sfi.CopyTo(Path.Combine(target.FullName, sfi.Name), true);
            }

            foreach (DirectoryInfo sdi in source.GetDirectories()) {
                DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(sdi.Name);
                CopyAll(sdi, nextTargetSubDir);
            }
        }

        private void Copy(FileInfo file, DirectoryInfo target) => file.CopyTo(Path.Combine(target.FullName, file.Name), true);

        protected override void SelectNextEntry() {
            List<Row> nonEmptyRows = new List<Row>();
            foreach (Row row in rows) {
                if (!(row is EmptyDataRow))
                    nonEmptyRows.Add(row);
            }

            int index = nonEmptyRows.IndexOf(rows.Find(x => x.isActive));

            if (nonEmptyRows.Count != 0) {
                if (index != nonEmptyRows.Count - 1) {
                    nonEmptyRows[index].DeActivate();
                    nonEmptyRows[index + 1].Activate();
                } else {
                    nonEmptyRows[index].DeActivate();
                    nonEmptyRows.FirstOrDefault().Activate();
                }
            }
        }

        protected override void SelectPreviousEntry() {
            List<Row> nonEmptyRows = new List<Row>();
            foreach (Row row in rows) {
                if (!(row is EmptyDataRow))
                    nonEmptyRows.Add(row);
            }

            int index = nonEmptyRows.IndexOf(rows.Find(x => x.isActive));

            if (nonEmptyRows.Count != 0) {
                if (index == 0) {
                    nonEmptyRows[index].DeActivate();
                    nonEmptyRows[nonEmptyRows.Count - 1].Activate();
                } else {
                    nonEmptyRows[index].DeActivate();
                    nonEmptyRows[index - 1].Activate();
                }
            }
        }

        public override void Draw() {
            renderer.DrawComponentBorders(position, size, colors.Active);

            WritableData wData = new WritableData {
                backgroundColor = colors.Background,
                textColor = colors.Text,

                data = "Name".PadRight(size.X / 2),
                position = (position.X + 1, position.Y + 1)
            };
            renderer.Write(wData);

            wData.data = " Size   ";
            wData.position = (position.X + size.X / 2 + 1, position.Y + 1);
            renderer.Write(wData);

            wData.data = "Last Access     ";
            wData.position = (position.X + size.X / 2 + 9, position.Y + 1);
            renderer.Write(wData);

            for (int i = 0; i < rows.Count; i++) {
                wData.position = (position.X + 1, position.Y + 2 + i);

                wData.backgroundColor = rows[i].isActive ? colors.Active : colors.Background;

                wData.data = rows[i].View();
                renderer.Write(wData);
            }
        }
    }
}
