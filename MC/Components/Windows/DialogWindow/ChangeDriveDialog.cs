﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MC {
    class ChangeDriveDialog : DialogWindow {
        private readonly Application app;
        private List<DriveInfo> drives;

        public ChangeDriveDialog((int, int) position, (int, int) size, Renderer renderer, Colors colors,
            Application app) : base(position, size, renderer, colors) {
            this.app = app;
            Initialize();
        }

        protected override List<Row> CreateRows() {
            var rows = new List<Row>();
            this.drives = new List<DriveInfo>();

            var drives = DriveInfo.GetDrives().ToList();
            foreach (var item in drives) {
                if (item.IsReady)
                    this.drives.Add(item);
            }

            foreach (var drive in this.drives) {
                rows.Add(new TextRow(size.X - 2, drive.Name));
            }

            while (rows.Count < size.Y - 2) {
                rows.Add(new EmptyTextRow(size.X - 2));
            }

            return rows;
        }

        protected override void Action() {
            app.GetActiveViewWindow().selectedDirectory = new DirectoryInfo(drives[rows.IndexOf(rows.Find(x => x.isActive))].Name);
            app.RefreshWindows();
        }
    }
}
