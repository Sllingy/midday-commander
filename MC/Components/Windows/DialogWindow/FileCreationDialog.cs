﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace MC {
    class FileCreationDialog : DialogWindow {
        private readonly Application app;
        private string data = "";
        private readonly bool createFile;

        public FileCreationDialog((int, int) position, (int, int) size, Renderer renderer, Colors colors,
            Application app, bool createFile) : base(position, size, renderer, colors) {
            this.app = app;
            this.createFile = createFile;
            Initialize();
        }

        protected override List<Row> CreateRows() {
            var rows = new List<Row> {
                new TextRow(size.X - 2, data)
            };

            while (rows.Count < size.Y - 2) {
                rows.Add(new EmptyTextRow(size.X - 2));
            }

            return rows;
        }

        public string GetWriteableCharacter(char input) {
            try {
                return Regex.Replace(input.ToString(), @"[^\w\.@-]", "", RegexOptions.None, TimeSpan.FromSeconds(1.5));
            } catch (RegexMatchTimeoutException) {
                return String.Empty;
            }
        }

        protected void WriteCharacter(char input) {
            string s = GetWriteableCharacter(input);
            if (s != String.Empty) {
                data += s;
            }
            rows = CreateRows();
        }

        protected void RemoveCharacter() {
            if (data.Length > 0) {
                data = data.Substring(0, data.Length - 1);
                rows = CreateRows();
            }
        }

        protected override void Action() {
            if (createFile)
                app.GetActiveViewWindow().CreateFile(data);
            else
                app.GetActiveViewWindow().CreateDirectory(data);
            app.GetActiveViewWindow().RecreateRows();
        }

        public override void Draw() {
            renderer.DrawComponentBorders(position, size, colors.Active);

            WritableData wData = new WritableData {
                backgroundColor = colors.Background,
                textColor = colors.Text
            };
            wData.backgroundColor = colors.Background;

            for (int i = 0; i < rows.Count; i++) {
                wData.position = (position.X + 1, position.Y + 1 + i);
                wData.data = rows[i].View();
                renderer.Write(wData);
            }
        }

        public override void HandleInput(ConsoleKeyInfo keyInfo) {
            if (isActive) {
                switch (keyInfo.Key) {
                    case ConsoleKey.Enter:
                        Action();
                        break;
                    case ConsoleKey.Backspace:
                        RemoveCharacter();
                        break;
                    default:
                        WriteCharacter(keyInfo.KeyChar);
                        break;
                }
            }
        }
    }
}
