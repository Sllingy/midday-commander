﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MC {
    class Game : Component {
        private readonly BufferChar[,] buffer;
        private readonly Application app;
        private GameBlock activeGameBlock;
        private readonly List<GameBlock> blocks = new List<GameBlock>();
        private readonly List<int> numbers = new List<int>() { 1, 2, 3, 4, 5, 6, 10, 12 };
        private readonly List<BufferChar[,]> shapes = new List<BufferChar[,]>();
        private int blockNumber = 0;
        private int score = 0;
        private int gravityTimer = 0;
        private int gravityInterval = 30;
        private int shapeCounter;
        private bool paused;

        public Game((int, int) position, (int X, int Y) size, Renderer renderer, Colors colors, Application app) : base(position, size, renderer, colors) {
            this.app = app;
            buffer = new BufferChar[size.X - 2, size.Y - 2];
            ClearBuffer();
            GenerateShapes(100);
            AddBlock();
            this.app.CreateLabels(new List<string> { "Left/Right Arrow - Move Block", "Up Arrow - Decrease Game Speed", "Down Arrow - Increase Game Speed", "Backspace - Delete Block",
                "Tab - Return to Window", "Escape - Exit" });
        }

        public override void Close() {
            app.CreateLabels(new List<string> { "F1 - Create File", "F2 - Create Directory", "F3 - Copy File/Directory",
                "F4 - Delete File/Directory", "F5 - Change Drive", "F6 - Edit File",
                "F10 - Exit", "F12 - TOTRIS"});
        }

        public void AddNextBlock() {
            if (FloorCollision(activeGameBlock) || BottomBlockCollision(activeGameBlock)) {
                AddBlock();
            }
        }

        private ConsoleColor GetRandomColor() {
            Random rnd = new Random();
            int o = rnd.Next(15);
            return (ConsoleColor)o;
        }

        public BufferChar[,] GetShape(int x, int y) {
            BufferChar b = new BufferChar {
                BackgroundColor = colors.Background,
                TextColor = GetRandomColor(),
                Data = '█'
            };

            BufferChar[,] shape = new BufferChar[x, y];

            for (int i = 0; i < x; i++) {
                for (int n = 0; n < y; n++) {
                    shape[i, n] = b;
                }
            }

            return shape;
        }

        public BufferChar[,] GetShape(int x, int y, ConsoleColor consoleColor) {
            BufferChar b = new BufferChar {
                BackgroundColor = colors.Background,
                TextColor = consoleColor,
                Data = '█'
            };

            BufferChar[,] shape = new BufferChar[x, y];

            for (int i = 0; i < x; i++) {
                for (int n = 0; n < y; n++) {
                    shape[i, n] = b;
                }
            }

            return shape;
        }

        private void GenerateShapes(int shapeCount) {
            int counter = 0;
            while (counter < shapeCount) {
                Random rnd = new Random();
                int a = rnd.Next(1, 10);
                int x, y;

                BufferChar[,] shape = { };

                x = rnd.Next(numbers.Count);
                y = rnd.Next(1, numbers.Count);

                if (a < 5) {
                    shape = GetShape(numbers[x] * 2, numbers[y / 4]);
                }
                if (a >= 5) {
                    shape = GetShape(numbers[x / 2], y);
                }

                shapes.Add(shape);
                counter++;

                Thread.Sleep(15);
            }
        }

        public void AddBlock() {
            BufferChar[,] shape = shapes[shapeCounter];
            int length0 = shape.GetLength(0);
            int length1 = shape.GetLength(1);
            GameBlock gameBlock = new GameBlock(((size.X - length0) / 2, length1 + 2), shape, this);
            blocks.Add(gameBlock);
            activeGameBlock = gameBlock;

            if (shapeCounter != shapes.Count - 1)
                shapeCounter++;
            else shapeCounter = 0;

            blockNumber++;
        }

        private void DeleteLastShape() {
            if (blocks.Count > 1) {
                blocks.RemoveAt(blocks.Count - 2);
            }
        }

        private bool FloorCollision(GameBlock block) => block.position.Y + 1 > buffer.GetLength(1);

        private bool BottomBlockCollision(GameBlock block1) {
            for (int i = 0; i < block1.shape.GetLength(0); i++) {
                foreach (var block in blocks) {
                    if (block1 != block) {
                        for (int a = 0; a < block.shape.GetLength(0); a++) {
                            if (block1.position.X + i == block.position.X + a && block1.position.Y == block.position.Y - block.shape.GetLength(1))
                                return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool LeftWallCollision(GameBlock block) => block.position.X - 1 < 0;

        private bool RightWallCollision(GameBlock block) => block.position.X + block.shape.GetLength(0) == buffer.GetLength(0);

        private bool BlockRightCollision(GameBlock block1) {
            for (int i = 0; i < block1.shape.GetLength(1); i++) {
                foreach (var block in blocks) {
                    if (block1 != block) {
                        for (int a = 0; a < block.shape.GetLength(1); a++) {
                            if ((block1.position.Y - i) == block.position.Y - a) {
                                if (((block1.position.X + block1.shape.GetLength(0) - 1) + 1) == block.position.X)
                                    return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        private bool BlockLeftCollision(GameBlock block1) {
            for (int i = 0; i < block1.shape.GetLength(1); i++) {
                foreach (var block in blocks) {
                    if (block1 != block) {
                        for (int a = 0; a < block.shape.GetLength(1); a++) {
                            if ((block1.position.Y - i) == block.position.Y - a) {
                                if ((block1.position.X - 1) == block.position.X + block.shape.GetLength(0) - 1)
                                    return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        private void ClearBuffer() {
            BufferChar bChar = new BufferChar {
                BackgroundColor = colors.Background,
                TextColor = colors.Text,
                Data = ' '
            };

            for (int i = 0; i < buffer.GetLength(0); i++) {
                for (int n = 0; n < buffer.GetLength(1); n++) {
                    buffer[i, n].Data = bChar.Data;
                    buffer[i, n].BackgroundColor = bChar.BackgroundColor;
                    buffer[i, n].TextColor = bChar.TextColor;
                }
            }
        }

        private void CheckWholeRow() {
            bool fullRow = true;
            for (int i = 0; i < buffer.GetLength(0); i++) {
                if (buffer[i, buffer.GetLength(1) - 1].TextColor == colors.Text)
                    fullRow = false;
            }
            if (fullRow) {
                score++;
                for (int i = blocks.Count - 1; i >= 0; i--) {
                    if (blocks[i].position.Y == buffer.GetLength(1)) {
                        if (blocks[i].shape.GetLength(1) == 1) {
                            blocks.RemoveAt(i);
                        } else {
                            if (blocks[i].shape[0, 0] != null) {
                                blocks[i].shape = GetShape(blocks[i].shape.GetLength(0), blocks[i].shape.GetLength(1) - 1, blocks[i].shape[0, 0].TextColor);
                            }
                        }
                    }
                }
            }
        }

        public void Update() {
            if (!paused && isActive) {
                Gravity();
                AddNextBlock();
                CheckWholeRow();
            }
        }

        private void Gravity() {
            if (gravityTimer == 0) {
                bool floorCollision;
                bool bottomBCollison;

                foreach (var block in blocks) {
                    floorCollision = false;
                    bottomBCollison = false;
                    bottomBCollison = BottomBlockCollision(block);
                    floorCollision = FloorCollision(block);

                    if (!floorCollision && !bottomBCollison)
                        block.position.Y++;
                }
                gravityTimer = gravityInterval;
            }
            gravityTimer--;
        }

        private void IncreaseGravityInterval() => gravityInterval += 15;

        private void DecreaseGravityInterval() {
            if (gravityInterval > 15)
                gravityInterval -= 15;
        }

        public void DrawBlocks() {
            foreach (var block in blocks) {
                block.Draw();
            }
        }

        public void DrawBackground() {
            BufferChar bChar = new BufferChar {
                Data = ' ',
                BackgroundColor = colors.Background,
                TextColor = colors.Text
            };

            for (int i = 0; i < buffer.GetLength(0); i++) {
                for (int n = 0; n < buffer.GetLength(1); n++) {
                    WriteCharacter(bChar, i, n);
                }
            }
        }

        protected void DrawName() {
            WritableData writeableData = new WritableData {
                backgroundColor = colors.Background,
                textColor = colors.Text,
                data = "TOTRIS"
            };
            writeableData.position = (((buffer.GetLength(0) - writeableData.data.Length) / 2), 10);
            Write(writeableData);

            writeableData.data = "BlockCount: " + blockNumber;
            writeableData.position = (((buffer.GetLength(0) - writeableData.data.Length) / 2), 11);
            Write(writeableData);

            writeableData.data = "Score: " + score;
            writeableData.position = (((buffer.GetLength(0) - writeableData.data.Length) / 2), 12);
            Write(writeableData);
        }

        public void TogglePause() => paused = !paused;

        public void WriteCharacter(BufferChar bChar, int posX, int posY) {
            try {
                buffer[posX, posY].Data = bChar.Data;
                buffer[posX, posY].BackgroundColor = bChar.BackgroundColor;
                buffer[posX, posY].TextColor = bChar.TextColor;
            } catch (Exception) { }
        }

        public void Write(WritableData writeableData) {
            for (int i = 0; i < writeableData.data.Length; i++) {
                try {
                    buffer[writeableData.position.X + i, writeableData.position.Y].Data = writeableData.data[i];
                    buffer[writeableData.position.X + i, writeableData.position.Y].BackgroundColor = writeableData.backgroundColor;
                    buffer[writeableData.position.X + i, writeableData.position.Y].TextColor = writeableData.textColor;
                } catch (Exception) { }
            }
        }

        public override void Draw() {
            renderer.DrawComponentBorders(position, size, colors.Active);
            DrawBackground();
            DrawBlocks();
            DrawName();
            DrawToMainBuffer();
        }

        public void DrawToMainBuffer() {
            BufferChar bChar = new BufferChar();

            for (int i = 0; i < buffer.GetLength(0); i++) {
                for (int n = 0; n < buffer.GetLength(1); n++) {
                    bChar.Data = buffer[i, n].Data;
                    bChar.BackgroundColor = buffer[i, n].BackgroundColor;
                    bChar.TextColor = buffer[i, n].TextColor;

                    renderer.WriteCharacter(bChar, position.X + 1 + i, position.Y + 1 + n);
                }
            }
        }

        protected void Move(Direction direction) {
            switch (direction) {
                case Direction.LEFT:
                    bool leftBlockCollision;
                    bool leftWallCollision;

                    leftBlockCollision = BlockLeftCollision(activeGameBlock);
                    leftWallCollision = LeftWallCollision(activeGameBlock);

                    if (!leftBlockCollision && !leftWallCollision)
                        activeGameBlock.position.X--;
                    break;
                case Direction.RIGHT:
                    bool rightBlockCollision;
                    bool rightWallCollision;

                    rightBlockCollision = BlockRightCollision(activeGameBlock);
                    rightWallCollision = RightWallCollision(activeGameBlock);

                    if (!rightBlockCollision && !rightWallCollision)
                        activeGameBlock.position.X++;
                    break;
            }
        }

        private void FlipBlock(GameBlock block) {
            BufferChar[,] buffer = block.shape;
            (int ,int) p = block.position;

            int middle = (block.shape.GetLength(0) - 1) / 2;
            int middleX = block.position.X + middle;

            if (block.shape.GetLength(0) > block.shape.GetLength(1)) {
                middle = (block.shape.GetLength(1) - 1) / 2;
                int middleY = block.position.Y + middle;

                if (block.shape.GetLength(0) > 1) {
                    block.shape = GetShape(block.shape.GetLength(1) * 2, block.shape.GetLength(0) / 2, block.shape[0, 0].TextColor);
                }

                block.position.X = middleX - ((block.shape.GetLength(0) - 1) / 2);
                block.position.Y = middleY + ((block.shape.GetLength(1) - 1) / 2);
            } else {
                middle = (block.shape.GetLength(1) - 1) / 2;
                int middleY = block.position.Y - middle;

                if (block.shape.GetLength(0) > 1) {
                    block.shape = GetShape(block.shape.GetLength(1) * 2, block.shape.GetLength(0) / 2, block.shape[0, 0].TextColor);
                }

                block.position.X = middleX - ((block.shape.GetLength(0) - 1) / 2);
                block.position.Y = middleY + ((block.shape.GetLength(1) - 1) / 2);
            }

            if (block.position.X < 0) {
                block.shape = buffer;
                block.position = p;
            }
        }

        public override void HandleInput(ConsoleKeyInfo keyInfo) {
            if (isActive && !paused) {
                switch (keyInfo.Key) {
                    case ConsoleKey.DownArrow:
                        DecreaseGravityInterval();
                        break;
                    case ConsoleKey.UpArrow:
                        IncreaseGravityInterval();
                        break;
                    case ConsoleKey.LeftArrow:
                        Move(Direction.LEFT);
                        break;
                    case ConsoleKey.RightArrow:
                        Move(Direction.RIGHT);
                        break;
                    case ConsoleKey.Spacebar:
                        FlipBlock(activeGameBlock);
                        break;
                    case ConsoleKey.Backspace:
                        DeleteLastShape();
                        break;
                }
            }

            if (isActive && keyInfo.Key == ConsoleKey.Enter) {
                TogglePause();
            }

            base.HandleInput(keyInfo);
        }
    }
}
