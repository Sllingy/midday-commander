﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC {
    class GameBlock {
        public BufferChar[,] shape;
        public (int X, int Y) position;
        protected Game game;

        public GameBlock((int, int) position, BufferChar[,] shape, Game game) {
            this.shape = shape;
            this.game = game;
            this.position = position;
        }

        public void Draw() {
            for (int i = 0; i < shape.GetLength(0); i++) {
                for (int n = shape.GetLength(1); n > 0; n--) {
                    game.WriteCharacter(shape[i, n - 1], position.X + i, position.Y - n);
                }
            }
        }
    }
}
