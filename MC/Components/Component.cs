﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC {
    abstract class Component {
        public (int X, int Y) size;
        public (int X, int Y) position;
        protected Renderer renderer;
        public Colors colors;
        public bool isActive;

        public abstract void Draw();

        public virtual void Activate() => isActive = true;
        public virtual void DeActivate() => isActive = false;
        public virtual void Initialize() { }
        public virtual void HandleInput(ConsoleKeyInfo key) { }
        public virtual void Close() { }

        public Component((int, int) position, (int, int) size, Renderer renderer, Colors colors) {
            this.position = position;
            this.size = size;
            this.renderer = renderer;
            this.colors = colors;
        }
    }
}
