﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC {
    class Colors {
        public ConsoleColor Background;
        public ConsoleColor Text;
        public ConsoleColor Active;
        public ConsoleColor Inactive;

        public Colors(ConsoleColor background, ConsoleColor text, ConsoleColor active, ConsoleColor inactive) {
            Background = background;
            Text = text;
            Active = active;
            Inactive = inactive;
        }
    }
}
