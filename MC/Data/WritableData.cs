﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC {
    class WritableData {
        public (int X, int Y) position;
        public string data;
        public ConsoleColor backgroundColor;
        public ConsoleColor textColor;
    }
}
