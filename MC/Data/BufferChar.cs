﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC {
    struct BufferChar {
        public static bool operator ==(BufferChar lhs, BufferChar rhs) {
            return lhs.Data == rhs.Data && lhs.TextColor == rhs.TextColor && lhs.BackgroundColor == rhs.BackgroundColor;
        }

        public static bool operator !=(BufferChar lhs, BufferChar rhs) {
            return !(lhs == rhs);
        }

        public char Data;
        public ConsoleColor TextColor;
        public ConsoleColor BackgroundColor;
    }
}
