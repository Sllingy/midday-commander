﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC {
    class TextRow : Row {
        private readonly string data;

        public TextRow(int length, string data) : base(length) => this.data = data;

        public override string View() => data.PadRight(length);
    }
}
