﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC {
    class EmptyTextRow : TextRow {
        public EmptyTextRow(int length) : base(length, "") {

        }
    }
}
