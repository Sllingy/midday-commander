﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MC {
    class DirectoryRow : DataRow {
        public DirectoryRow(FileSystemInfo fileInfo, int length) : base(fileInfo, length) {
            name = fileInfo.Name.Length > (length - 1) / 2
                ? ("/" + fileInfo.Name.Substring(0, ((length - 1) / 2) - 2) + "..").PadRight(length / 2)
                : ("/" + fileInfo.Name).PadRight(length / 2);

            size = "       ";

            date = (fileInfo.LastAccessTime.ToShortTimeString() + " " +
                fileInfo.LastAccessTime.ToShortDateString()).PadLeft(length / 2 - 9);
        }
    }
}
