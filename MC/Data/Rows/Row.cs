﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC {
    abstract class Row {
        public bool isActive = false;
        protected int length;

        public Row(int length) => this.length = length;

        public void Activate() => isActive = true;

        public void DeActivate() => isActive = false;

        public abstract string View();
    }
}
