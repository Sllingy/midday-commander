﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MC {
    class EmptyDataRow : DataRow {
        public EmptyDataRow(int length) : base(new DirectoryInfo(@"C:/"), length) {
            name = "".PadRight(length / 2);
            size = "       ";
            date = "".PadRight(length / 2 - 9);
        }
    }
}
