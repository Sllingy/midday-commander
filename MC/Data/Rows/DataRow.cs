﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MC {
    abstract class DataRow : Row {
        public FileSystemInfo fileInfo;
        public string name, size, date;

        public DataRow(FileSystemInfo fileInfo, int lenght) : base(lenght) => this.fileInfo = fileInfo;

        public override string View() => name + "│" + size + "│" + date;
    }
}
