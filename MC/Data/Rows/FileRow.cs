﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MC {
    class FileRow : DataRow {
        public FileRow(FileSystemInfo fileInfo, int length) : base(fileInfo, length) {
            name = fileInfo.Name.Length > length / 2
                ? (fileInfo.Name.Substring(0, (length / 2) - 2) + "..").PadRight(length / 2)
                : fileInfo.Name.PadRight(length / 2);

            if (fileInfo is FileInfo f)
                size = BytesToString(f.Length).PadLeft(7);

            date = (fileInfo.LastAccessTime.ToShortTimeString() + " " +
                fileInfo.LastAccessTime.ToShortDateString()).PadLeft(length / 2 - 9);
        }

        protected string BytesToString(long byteCount) {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" };
            if (byteCount == 0)
                return "0" + suf[0];
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            string s = (Math.Sign(byteCount) * num).ToString() + suf[place];
            if (s.Length > 7)
                s = s.Substring(0, 7);
            return s;
        }
    }
}
