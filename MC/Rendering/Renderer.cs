﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC {
    class Renderer {
        private BufferChar[,] bufferFront;
        private BufferChar[,] bufferBack;
        private readonly Colors colors;

        public Renderer(int consoleWidth, int consoleHeight, Colors colors) {
            Console.BackgroundColor = colors.Background;
            Console.ForegroundColor = colors.Text;
            Console.CursorVisible = false;
            Console.Title = "MiddayCommander";
            Console.SetWindowSize(consoleWidth, consoleHeight);
            Console.Clear();

            bufferFront = new BufferChar[consoleWidth, consoleHeight];
            bufferBack = new BufferChar[consoleWidth, consoleHeight];
            this.colors = colors;
        }

        public void DrawToConsole() {
            for (int i = 0; i < bufferFront.GetLength(0); i++) {
                for (int n = 0; n < bufferFront.GetLength(1); n++) {
                    if (bufferFront[i, n] != bufferBack[i, n]) {
                        Console.SetCursorPosition(i, n);
                        Console.BackgroundColor = bufferFront[i, n].BackgroundColor;
                        Console.ForegroundColor = bufferFront[i, n].TextColor;
                        Console.Write(bufferFront[i, n].Data);                        
                    }
                }
            }
            var temp = bufferFront;
            bufferFront = bufferBack;
            bufferBack = temp;
        }        

        public void DrawComponentBorders((int X, int Y) position, (int X, int Y) size, ConsoleColor textColor) {
            BufferChar bChar = new BufferChar {
                BackgroundColor = colors.Background,
                TextColor = textColor,
                Data = '█'
            };

            //Up
            for (int i = 0; i < size.X; i++) {
                WriteCharacter(bChar, position.X + i, position.Y);
            }

            //Left
            for (int i = 0; i < size.Y; i++) {
                WriteCharacter(bChar, position.X, position.Y + i);
            }

            //Right
            for (int i = 0; i < size.Y; i++) {
                WriteCharacter(bChar, position.X + size.X - 1, position.Y + i);
            }

            //Down
            for (int i = 0; i < size.X; i++) {
                WriteCharacter(bChar, position.X + i, position.Y + size.Y - 1);
            }
        }        

        public void Write(WritableData writeableData) {
            for (int i = 0; i < writeableData.data.Length; i++) {
                bufferFront[writeableData.position.X + i, writeableData.position.Y].Data = writeableData.data[i];
                bufferFront[writeableData.position.X + i, writeableData.position.Y].BackgroundColor = writeableData.backgroundColor;
                bufferFront[writeableData.position.X + i, writeableData.position.Y].TextColor = writeableData.textColor;
            }
        }

        public void WriteCharacter(BufferChar bChar, int posX, int posY) {
            bufferFront[posX, posY].Data = bChar.Data;
            bufferFront[posX, posY].BackgroundColor = bChar.BackgroundColor;
            bufferFront[posX, posY].TextColor = bChar.TextColor;
        }
    }
}
