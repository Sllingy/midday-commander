﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MC {
    class Application {
        private readonly List<Component> components = new List<Component>();
        private readonly Renderer renderer;
        private Colors _colors;
        private int activeWindowIndex;

        public Application() {
            SetColors(1);            
            renderer = new Renderer(248, 80, _colors);
            CreateWindows(4);
            CreateLabels(new List<string> { "F1 - Create File", "F2 - Create Directory", "F3 - Copy File/Directory",
                "F4 - Delete File/Directory", "F5 - Change Drive", "F6 - Edit File",
                "F10 - Exit", "F12 - TOTRIS"});
            ActivateFirstWindow();

            foreach (Component component in components) {
                if (component is ViewWindow viewWindow)
                    viewWindow.ActivateFirstRow();
            }
        }

        public void Run() {
            while (true) {
                if (Console.KeyAvailable)
                    HandleInput(Console.ReadKey(true));
                UpdateGame();
                Draw();
            }
        }

        public void SetColors(int colorMode) {
            switch (colorMode) {
                case 0:
                    _colors = new Colors(ConsoleColor.White, ConsoleColor.Black,
                        ConsoleColor.DarkCyan, ConsoleColor.Gray);
                    break;
                case 1:
                    _colors = new Colors(ConsoleColor.Black, ConsoleColor.White,
                        ConsoleColor.DarkCyan, ConsoleColor.DarkBlue);
                    break;
                case 2:
                    _colors = new Colors(ConsoleColor.Black, ConsoleColor.White,
                        ConsoleColor.Gray, ConsoleColor.DarkGray);
                    break;
                case 3:
                    _colors = new Colors(ConsoleColor.Black, ConsoleColor.DarkRed,
                        ConsoleColor.Gray, ConsoleColor.DarkGray);
                    break;
                case 4:
                    _colors = new Colors(ConsoleColor.DarkBlue, ConsoleColor.White,
                        ConsoleColor.Red, ConsoleColor.Blue);
                    break;
            }
        }

        private void CreateWindows(int windowCount) {
            int windowOffset = Console.WindowWidth / windowCount;

            for (int i = 0; i < windowCount; i++) {
                components.Add(
                    new ViewWindow(
                        (i * windowOffset, 0),
                        (Console.WindowWidth / windowCount, Console.WindowHeight - 1),
                        renderer,
                        _colors,
                        new DirectoryInfo(@"C:\"),
                        this
                        )
                );
            }
        }

        public void CreateLabels(List<string> data) {            
            components.RemoveAll(c => c is Label);

            int labelOffset = Console.WindowWidth / data.Count;

            for (int i = 0; i < data.Count; i++) {
                if (i == data.Count - 1) {
                    components.Add(
                    new Label(
                        (i * labelOffset, Console.WindowHeight - 1),
                        (Console.WindowWidth / data.Count, 1),
                        renderer,
                        _colors,
                        data[i].PadRight((Console.WindowWidth / data.Count) - 1)
                    )
                );
                } else {
                    components.Add(
                    new Label(
                        (i * labelOffset, Console.WindowHeight - 1),
                        (Console.WindowWidth / data.Count, 1),
                        renderer,
                        _colors,
                        data[i].PadRight(Console.WindowWidth / data.Count)
                    )
                );
                }
            }
        }

        public void CreateGame() {
            if (components.Find(x => x is Game) == null) {
                var viewWindows = components.Where(c => c is ViewWindow);

                ViewWindow activeViewWindow = (ViewWindow)viewWindows.Where(x => x.isActive).FirstOrDefault();

                if (activeViewWindow.size.X != 0) {
                    components.Add(
                    new Game(
                        (activeViewWindow.position.X, activeViewWindow.position.Y),
                        (activeViewWindow.size.X, activeViewWindow.size.Y),
                        renderer,
                        _colors,
                        this
                        )
                );
                }
            }
            foreach (Component component in components) {
                if (component is Game FileWindow) {
                    FileWindow.Activate();
                } else {
                    component.DeActivate();
                }
            }

        }

        public void CreateFileEditorWindow(FileInfo fileInfo) {
            components.Add(
                new FileEditor(
                    (0, 0),
                    (Console.WindowWidth, Console.WindowHeight - 1),
                    renderer,
                    _colors,
                    fileInfo,
                    this
                    )
            );

            foreach (Component component in components) {
                if (component is FileEditor FileWindow) {
                    FileWindow.Activate();
                } else {
                    component.DeActivate();
                }
            }
        }

        public void CreateDriveDialogWindow() {
            List<ViewWindow> viewWindows = new List<ViewWindow>();
            foreach (Component component in components) {
                if (component is ViewWindow viewWindow)
                    viewWindows.Add(viewWindow);
            }

            ViewWindow activeViewWindow = viewWindows.Find(v => v.isActive);
            int x = DriveInfo.GetDrives().ToList().FindAll(d => d.IsReady).Count;

            components.Add(
                new ChangeDriveDialog(
                    (activeViewWindow.position.X + 5, activeViewWindow.size.Y / 2),
                    (activeViewWindow.size.X - 10, x + 2),
                    renderer,
                    _colors,
                    this
                    )
            );            

            foreach (Component component in components) {
                if (component is DialogWindow dialogWindow) {
                    dialogWindow.Activate();
                    dialogWindow.ActivateFirstRow();
                } else {
                    component.DeActivate();
                }
            }
        }

        public void CreateFileCreationDialogWindow(bool createFile) {
            List<ViewWindow> viewWindows = new List<ViewWindow>();
            foreach (Component component in components) {
                if (component is ViewWindow viewWindow)
                    viewWindows.Add(viewWindow);
            }

            ViewWindow activeViewWindow = viewWindows.Find(z => z.isActive);

            components.Add(
                new FileCreationDialog(
                    (activeViewWindow.position.X + 5, activeViewWindow.size.Y / 2),
                    (activeViewWindow.size.X - 10, 3),
                    renderer,
                    _colors,
                    this,
                    createFile
                    )
            );

            foreach (Component component in components) {
                if (component is DialogWindow dialogWindow) {
                    dialogWindow.Activate();
                    dialogWindow.ActivateFirstRow();
                } else {
                    component.DeActivate();
                }
            }
        }

        public void CreateCopyDialogWindow() {

            var viewWindows = components.Where(c => c is ViewWindow);

            ViewWindow activeViewWindow = (ViewWindow)viewWindows.Where(x => x.isActive).FirstOrDefault();

            List<DirectoryInfo> directories = new List<DirectoryInfo>();
            foreach (ViewWindow viewWindow in viewWindows) {
                if (!viewWindow.isActive)
                    directories.Add(viewWindow.selectedDirectory);
            }
            int size = viewWindows.Count();

            components.Add(
                new CopyDialog(
                    (activeViewWindow.position.X + 5, activeViewWindow.size.Y / 2),
                    (activeViewWindow.size.X - 10, size + 2),
                    renderer,
                    _colors,
                    activeViewWindow.GetFileSystemInfo(),
                    directories,
                    this)
                );

            foreach (Component component in components) {
                if (component is DialogWindow dialogWindow) {
                    dialogWindow.Activate();
                    dialogWindow.ActivateFirstRow();
                } else {
                    component.DeActivate();
                }
            }
        }

        private void SetActiveWindowIndex() => activeWindowIndex = components.IndexOf(components.Find(x => x.isActive));

        private void ActivateIndexWindow() => components[activeWindowIndex].Activate();

        private void ActivateFirstWindow() {
            components.Find(w => w is ViewWindow).Activate();

            SetActiveWindowIndex();
        }

        private void SwitchActiveViewWindow() {
            List<Component> viewWindows = new List<Component>();
            foreach (Component component in components) {
                if (component is ViewWindow)
                    viewWindows.Add(component);
            }

            int index = viewWindows.IndexOf(components.Find(x => x.isActive));

            if (index != viewWindows.Count - 1) {
                viewWindows[index].DeActivate();
                viewWindows[index + 1].Activate();
            } else {
                viewWindows[index].DeActivate();
                viewWindows.FirstOrDefault().Activate();
            }

            SetActiveWindowIndex();
        }

        public void RefreshWindows() {
            foreach (var item in components) {
                if (item is ViewWindow viewWindow) {
                    viewWindow.RecreateRows();
                }
            }
        }

        public ViewWindow GetActiveViewWindow() => (ViewWindow)components[activeWindowIndex];

        public void UpdateGame() {
            foreach (var item in components) {
                if (item is Game game)
                    game.Update();
            }
        }

        public void DeleteAdditionalComponents() {
            DeleteDialog();
            DeleteFileEditor(true);
            DeleteGame();
        }

        public void DeleteDialog() {            
            var dialog = components.Where(c => c is DialogWindow).FirstOrDefault();
            if (dialog != null) {
                dialog.Close();
                components.Remove(dialog);
            }
        }

        public void DeleteFileEditor(bool save) {
            FileEditor editor = (FileEditor)components.Where(c => c is FileEditor).FirstOrDefault();            
                
            if(editor != null) {
                if (save) {
                    editor.SaveToFile();
                }
                editor.Close();
                components.Remove(editor);
            }            
        }

        public void DeleteGame() {
            Game game = (Game)components.Where(c => c is Game).FirstOrDefault();
            if(game != null) {
                game.Close();
                components.Remove(game);
            }            
        }

        public void Draw() {
            if (components.Count == 0)
                Console.Error.WriteLine("No components exist!");

            foreach (Component component in components) {
                if (component is Game) {
                    if (component.isActive)
                        component.Draw();
                } else {
                    component.Draw();
                }
            }

            renderer.DrawToConsole();
        }

        public void HandleInput(ConsoleKeyInfo keyInfo) {
            switch (keyInfo.Key) {
                case ConsoleKey.F10:
                    Environment.Exit(0);
                    break;
                case ConsoleKey.Tab:
                    if (components.Find(x => x.isActive) is ViewWindow)
                        SwitchActiveViewWindow();                    
                    if (components.Find(x => x.isActive) is Game game) {
                        game.DeActivate();
                        ActivateIndexWindow();
                        SwitchActiveViewWindow();
                    }
                    break;
                case ConsoleKey.F3:
                    CreateCopyDialogWindow();
                    break;
                case ConsoleKey.F5:
                    CreateDriveDialogWindow();
                    break;
                case ConsoleKey.F9:
                    DeleteFileEditor(false);
                    ActivateIndexWindow();
                    break;
                case ConsoleKey.F12:
                    CreateGame();
                    break;
                case ConsoleKey.Escape:
                    DeleteAdditionalComponents();
                    ActivateIndexWindow();
                    break;
                default:
                    for (int i = components.Count - 1; i >= 0; i--) {
                        components[i].HandleInput(keyInfo);
                    }
                    break;
            }
        }
    }
}
